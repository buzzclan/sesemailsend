package com.buzz.taq.email.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "maillist" })
public class SendMailList {
	@JsonProperty("maillist")
	private List<SendMail> mailList = null;

	@JsonProperty("maillist")
	public List<SendMail> getsendMail() {
		return mailList;
	}

	@JsonProperty("maillist")
	public void setSendMail(List<SendMail> a) {
		this.mailList = a;
	}
}
