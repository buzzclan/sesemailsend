package com.buzz.taq.emailResopnse.entity;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "subjectEntityID", "subjectEntityType", "objectEntityID", "objectEntityType",
		"templateID", "emailTO", "sentBy" })
public class MsgBase {
	@JsonProperty("subjectEntityID")
	private BigDecimal subjectEntityID;
	@JsonProperty("subjectEntityType")
	private String subjectEntityType;
	@JsonProperty("objectEntityID")
	private BigDecimal objectEntityID;
	@JsonProperty("objectEntityType")
	private String objectEntityType;
	@JsonProperty("templateID")
	private String templateID;
	@JsonProperty("emailTO")
	private String emailTO;
	@JsonProperty("sentBy")
	private String sentBy;
	
	public void readFromInMsg(MsgBase sm) {
		
		this.objectEntityID = sm.objectEntityID;
		this.objectEntityType = sm.objectEntityType;
		
		this.subjectEntityType = sm.subjectEntityType;
		this.subjectEntityID = sm.subjectEntityID;
		
		this.templateID = sm.templateID;
		this.emailTO = sm.emailTO;
		this.sentBy = sm.sentBy;
	}

	
	@JsonProperty("subjectEntityID")
	public BigDecimal getSubjectEntityID() {
		return subjectEntityID;
	}

	@JsonProperty("subjectEntityID")
	public void setSubjectEntityID(BigDecimal subjectEntityID) {
		this.subjectEntityID = subjectEntityID;
	}

	@JsonProperty("subjectEntityType")
	public String getSubjectEntityType() {
		return subjectEntityType;
	}

	@JsonProperty("subjectEntityType")
	public void setSubjectEntityType(String subjectEntityType) {
		this.subjectEntityType = subjectEntityType;
	}

	@JsonProperty("objectEntityID")
	public BigDecimal getObjectEntityID() {
		return objectEntityID;
	}

	@JsonProperty("objectEntityID")
	public void setObjectEntityID(BigDecimal objectEntityID) {
		this.objectEntityID = objectEntityID;
	}

	@JsonProperty("objectEntityType")
	public String getObjectEntityType() {
		return objectEntityType;
	}

	@JsonProperty("objectEntityType")
	public void setObjectEntityType(String objectEntityType) {
		this.objectEntityType = objectEntityType;
	}

	@JsonProperty("templateID")
	public String getTemplateID() {
		return templateID;
	}

	@JsonProperty("templateID")
	public void setTemplateID(String templateID) {
		this.templateID = templateID;
	}

	@JsonProperty("emailTO")
	public String getEmailTO() {
		return emailTO;
	}

	@JsonProperty("emailTO")
	public void setEmailTO(String emailTO) {
		this.emailTO = emailTO;
	}

	@JsonProperty("sentBy")
	public String getSentBy() {
		return sentBy;
	}

	@JsonProperty("sentBy")
	public void setSentBy(String sentBy) {
		this.sentBy = sentBy;
	}
	
	
}
