package com.buzz.taq.email.SESMailSend;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.buzz.taq.email.SESEmail;
import com.buzz.taq.email.entity.SendMail;
import com.buzz.taq.email.entity.SendMailList;
import com.buzz.taq.emailResopnse.entity.MailResponse;
import com.buzz.taq.emailResopnse.entity.ReturnMessage;
import com.buzz.taq.utils.Profiler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class SESMultiMail
 */
@WebServlet("/SESMultiMail")
public class SESMultiMail extends HttpServlet {
	private static final Logger LOGGER = LogManager.getLogger(SESMultiMail.class.getName());
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SESMultiMail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Configurator.setLevel("org.apache.http", Level.WARN);
		LOGGER.info("In call");
		LOGGER.error("In call");
		LOGGER.fatal("In call");
		List<SendMail> smList = readBody(request, response);
		MailResponse mr = new MailResponse();
		int i = 0;
		for (SendMail sm : smList) {
			ReturnMessage rmObj = new ReturnMessage();
			rmObj.readFromInMsg(sm);

			if (sm.isSent()) {
				rmObj.setMsgId(sm.getMsgID());
			} else {
				rmObj.setMsgId(sm.getMsgID());
				LOGGER.error("The email was not sent. Error message:" + sm.getMsgID());
				i++;
			}
			mr.getMessages().add(rmObj);
			if (i > 0) {
				mr.setError("Error:" + i);
			}
		}
		mr.setStatus(Profiler.getInstance().toString()); // "Attempted:" + mr.getMessages().size() + ' ');
		
		response.getWriter().append(mr.ToJSON(true));
		response.setStatus(HttpServletResponse.SC_OK);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private List<SendMail> readBody(HttpServletRequest httpRequest, HttpServletResponse response) throws IOException {
		List<SendMail> retVal = new ArrayList<SendMail>();
		String JSONStr = httpRequest.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

		LOGGER.trace(JSONStr);

		ObjectMapper mapper = new ObjectMapper();
		Profiler.getInstance().start("SendMultiEmail");
		try {
			// JSON file to Java object
			SendMailList smOBJ = mapper.readValue(JSONStr, SendMailList.class);
			List<SendMail> smL = smOBJ.getsendMail();
			for (int i = 0; i < smL.size(); i++) {
				Profiler.getInstance().start("SendEachEmail");
				SendMail sm = SESEmail.sendEmail(smL.get(i));
				Profiler.getInstance().stop("SendEachEmail");
				LOGGER.trace(sm.getMsgID());
				retVal.add(sm);
			}
		} catch (JsonParseException | JsonMappingException e) {
			response.getWriter().append("Bad Json inputed>> " + JSONStr + "\n" + e.getMessage());
			LOGGER.error("Bad Json inputed>> " + JSONStr);
		} catch (IOException e) {
			response.getWriter().append("IO Error while parsing request:\n" + e.getMessage());
			LOGGER.error("IO Error while parsing request", e);
		} catch (Exception e) {
			response.getWriter().append("Error while parsing request :\n" + e.getMessage());
		}

		Profiler.getInstance().stop("SendMultiEmail");
		//response.getWriter().append(Profiler.getInstance().toString());
		return retVal;// ;httpRequest.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

	}

}
