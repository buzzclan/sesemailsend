//-----------------------------------com.buzz.taq.emailResopnse.entity.MailResponse.java-----------------------------------

package com.buzz.taq.emailResopnse.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "status", "error", "messages" })
public class MailResponse {

	@JsonProperty("error")
	private String error;
	@JsonProperty("status")
	private String status;
	@JsonProperty("messages")
	private List<ReturnMessage> messages = new ArrayList<ReturnMessage>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}


	@JsonProperty("error")
	public String getError() {
		return error;
	}

	@JsonProperty("error")
	public void setError(String err) {
		this.error = err;
	}

	@JsonProperty("messages")
	public List<ReturnMessage> getMessages() {
		return messages;
	}

	@JsonProperty("messages")
	public void setMessages(List<ReturnMessage> messages) {
		this.messages = messages;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public String ToJSON(boolean prettyPrint) throws JsonProcessingException {
		String retVal = "";
		ObjectMapper mapper = new ObjectMapper();
		if (prettyPrint) {
			// Java objects to JSON string - pretty-print
			retVal = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} else {
			// Java objects to JSON string - compact-print
			retVal = mapper.writeValueAsString(this);
		}

		return retVal;
	}

}