//-----------------------------------com.buzz.taq.emailResopnse.entity.Message_.java-----------------------------------
package com.buzz.taq.emailResopnse.entity;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "msgId", "subjectEntityID", "subjectEntityType", "objectEntityID", "objectEntityType",
		"templateID", "emailTO", "sentBy" })
public class ReturnMessage extends MsgBase{

	@JsonProperty("msgId")
	private String msgId;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("msgId")
	public String getMsgId() {
		return msgId;
	}

	@JsonProperty("msgId")
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
	

}