package com.buzz.taq.email;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.buzz.taq.email.entity.SendMail;
import com.buzz.taq.utils.Profiler;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SESEmail {
	private static final Logger LOGGER = LogManager.getLogger(SESEmail.class.getName());

	static String defaultConfigSet = "ConfigSet";
	static String defaultReturnPath = "support@buzzclan.com";

	public static String SendEmail(EMail mailObj) throws AddressException {
		return sendEmail(mailObj.getSubject(), mailObj.getTxtPart(), mailObj.getHtmlPart(), mailObj.getFromMail(),
				mailObj.getTo(), mailObj.getCc(), mailObj.getBcc());
	}

	public static String sendEmail(String subject, String txtBody, String htmlBody, String from, String to, String cc,
			String bcc) throws AddressException {
		return sendEmail(subject, txtBody, htmlBody, from, parseReceipeintStr(to), parseReceipeintStr(cc),
				parseReceipeintStr(bcc));
	}

	public static String sendEmail(String configSet, String subject, String txtBody, String htmlBody, String from,
			String to, String cc, String bcc, String returnPath) throws AddressException {
		return sendEmail(configSet, subject, txtBody, htmlBody, from, parseReceipeintStr(to), parseReceipeintStr(cc),
				parseReceipeintStr(bcc), returnPath);
	}

	public static String sendEmail(String subject, String txtBody, String htmlBody, String from, String[] to,
			String[] cc, String[] bcc) {
		return sendEmail(defaultConfigSet, subject, txtBody, htmlBody, from, to, cc, bcc, defaultReturnPath);
	}

	public static SendMail sendEmail(SendMail smObj) {
		try {
			String msgID = sendEmail(smObj.getConfigSet(), smObj.getSubject(), smObj.getTxtBody(), smObj.getHtmlBody(),
					smObj.getFrom(), smObj.getTo(), smObj.getCc(), smObj.getBcc(), smObj.getReturnPath());
			smObj.setSent(true);
			smObj.setMsgID(msgID);
		} catch (Exception e) {
			LOGGER.error("Error while sending email:", e);
			smObj.setSent(false);
			smObj.setMsgID(e.getMessage());
		}
		return smObj;
	}

	public static String sendEmail(String configSet, String subject, String txtBody, String htmlBody, String from,
			String[] to, String[] cc, String[] bcc, String returnPath) {

		String retVal = "";
		// Construct an object to contain the recipient address(s).
		Destination destination = new Destination();
		boolean targetPresent = false;
		if (to != null && to.length > 0) {
			destination = destination.withToAddresses(to);
			targetPresent = true;
		}
		if (bcc != null && bcc.length > 0) {
			destination = destination.withBccAddresses(bcc);
			targetPresent = true;
		}
		if (cc != null && cc.length > 0) {
			destination = destination.withCcAddresses(cc);
			targetPresent = true;
		}
		if (targetPresent) {// if target present

			// Create the subject and body of the message.
			Content Subject = new Content().withData(subject);
			Content textBody = new Content().withData(txtBody);
			Content HTMLBody = new Content().withData(htmlBody);

			Body body = new Body().withText(textBody).withHtml(HTMLBody);

			// Create a message with the specified subject and body.
			Message message = new Message().withSubject(Subject).withBody(body);

			Profiler.getInstance().start("Assemble the email");
			// Assemble the email.
			SendEmailRequest request = new SendEmailRequest().withSource(from).withDestination(destination)
					.withMessage(message).withReturnPath(returnPath).withReplyToAddresses(from)
					.withConfigurationSetName(configSet);
			try {
				LOGGER.trace("Attempting to send an email through Amazon SES by using the AWS SDK for Java...");

				authenticateSES();

				// Send the email.
				SendEmailResult result = client.sendEmail(request);
				String msgID = result.getMessageId();

				retVal = msgID;

			} catch (Exception ex) {
				LOGGER.error("The email was not sent. Error message: ", ex);
				retVal = "The email was not sent. Error message: " + ex.getMessage();
			}
			Profiler.getInstance().stop("Assemble the email");

		} else {
			LOGGER.error("\"The email was not sent. Error message:No one to send to! To, cc, bcc are all blank");
			retVal = "The email was not sent. Error message:No one to send to! To, cc, bcc are all blank";
		}
		return retVal;
	}

	static ProfileCredentialsProvider credentialsProvider;
	static AmazonSimpleEmailService client;

	private static ProfileCredentialsProvider authenticateSES() {
		if (credentialsProvider == null) {
			credentialsProvider = new ProfileCredentialsProvider();
			/*
			 * The ProfileCredentialsProvider will return your [Sachin-] credential profile
			 * by reading from the credentials file located at
			 * (C:\\Users\\sachi\\.aws\\credentials).
			 *
			 * TransferManager manages a pool of threads, so we create a single instance and
			 * share it throughout our application.
			 */
			try {
				credentialsProvider.getCredentials();
			} catch (Exception e) {
				String msg = "Cannot load the credentials from the credential profiles file. "
						+ "Please make sure that your credentials file is at the correct " + "location"
						+ System.getProperty("user.home") + ".aws/credentials, and is in valid format.";
				LOGGER.fatal(msg, e);
				throw new AmazonClientException(msg, e);
			}
			// Instantiate an Amazon SES client, which will make the service call with the
			// supplied AWS credentials.
			client = AmazonSimpleEmailServiceClientBuilder.standard().withCredentials(credentialsProvider)
					// Choose the AWS region of the Amazon SES endpoint you want to connect to. Note
					// that your production
					// access status, sending limits, and Amazon SES identity-related settings are
					// specific to a given
					// AWS region, so be sure to select an AWS region in which you set up Amazon
					// SES. Here, we are using
					// the US East (N. Virginia) region. Examples of other regions that Amazon SES
					// supports are US_WEST_2
					// and EU_WEST_1. For a complete list, see
					// http://docs.aws.amazon.com/ses/latest/DeveloperGuide/regions.html
					.withRegion("us-east-1").build();
		}
		return credentialsProvider;
	}

	private static String[] parseReceipeintStr(String receipient) throws AddressException {
		String[] recipientList = null;
		if (receipient != null && !receipient.trim().isEmpty()) {
			receipient.replace(';', ',');/// if the separator is ; change to ,
			recipientList = receipient.split(",");
			InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
			int counter = 0;
			for (String recipient : recipientList) {
				try {
					recipientAddress[counter] = new InternetAddress(recipient.trim());
				} catch (AddressException e) {
					LOGGER.error("Bad address detected:" + receipient, e);
					throw e;
				}
				recipientList[counter] = recipientAddress[counter].getAddress();
				counter++;
			}
		}
		return recipientList;
	}
}
