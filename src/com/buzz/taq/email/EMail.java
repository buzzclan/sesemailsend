package com.buzz.taq.email; 

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class EMail {
	private static final Logger LOGGER = LogManager.getLogger(EMail.class.getName());
	
	String fromName, fromMail, replyto, returnPath = "support@buzztaq.com", subject, txtPart, htmlPart;
	String[] to, cc, bcc;
	
	
	public EMail() {
		
	}
	public EMail(String fromMail, String fromName, String replyto, String returnPath, String to, String cc, String bcc, String subject,
			String txtPart, String htmlPart) throws AddressException, UnsupportedEncodingException {
		super();
		
		//InternetAddress fromAdd = new InternetAddress(fromMail, fromName); 
		this.fromName =  fromName;
		if (replyto != null && !replyto.isEmpty()) {
			this.replyto = replyto;
		} else {
			this.replyto = fromName;
		}
		if (returnPath != null && !returnPath.isEmpty()) {
			this.returnPath = returnPath;
		}
		this.to = parseReceipeintStr(to);
		this.cc = parseReceipeintStr(cc);
		this.bcc = parseReceipeintStr(bcc);
		checkDestinationEmpty();
		this.subject = subject;
		this.txtPart = txtPart;
		this.htmlPart = htmlPart;
	}

	public String sendEmail() throws AddressException {
		String retVal = "";
		if (replyto == null || replyto.isEmpty()) {
			this.replyto = fromName;
		}
		if (checkDestinationEmpty()) {
			retVal = SESEmail.SendEmail(this);
		}
		return retVal;
	}

	private boolean checkDestinationEmpty() throws AddressException {
		boolean retVal = true;
		if (this.to == null && this.cc == null && this.bcc != null)

		{
			LOGGER.error("No Destination. To, Cc, Bcc are all empty.");
			throw new AddressException("No Destination. To, Cc, Bcc are all empty.");
		}
		return retVal;
	}

	public EMail(String fromName, String fromMail, String replyto, String returnPath, String to, String subject, String txtPart,
			String htmlPart) throws AddressException, UnsupportedEncodingException {
		this(fromName, fromMail, replyto, returnPath, to, "", "", subject, txtPart, htmlPart);
	}

	public EMail(String fromName, String fromMail, String to, String subject, String txtPart, String htmlPart) throws AddressException, UnsupportedEncodingException {
		this(fromName, fromMail, "", "", to, "", "", subject, txtPart, htmlPart);
	}

	private static String[] parseReceipeintStr(String receipient) throws AddressException {
		String[] recipientList = null;
		if (receipient != null && !receipient.trim().isEmpty()) {
			receipient = receipient.replace(";", ",");/// if the separator is ; change to ,
			recipientList = receipient.split(",");
			InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
			int counter = 0;
			for (String recipient : recipientList) {
				try {
					recipientAddress[counter] = new InternetAddress(recipient.trim());
				} catch (AddressException e) {
					LOGGER.error("Bad address detected:" + recipient, e);
					throw e;
				}
				recipientList[counter] = recipientAddress[counter].getAddress();
				counter++;
			}
		}
		return recipientList;
	}

	/**
	 * @return the from
	 */
	public String getFromName() {
		return fromName;
	}

	/**
	 * @param from the from to set
	 */
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	/**
	 * @return the fromMail
	 */
	public String getFromMail() {
		return fromMail;
	}
	/**
	 * @param fromMail the fromMail to set
	 */
	public void setFromMail(String fromMail) {
		this.fromMail = fromMail;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(String[] to) {
		this.to = to;
	}
	/**
	 * @param cc the cc to set
	 */
	public void setCc(String[] cc) {
		this.cc = cc;
	}
	/**
	 * @param bcc the bcc to set
	 */
	public void setBcc(String[] bcc) {
		this.bcc = bcc;
	}
	/**
	 * @return the replyto
	 */
	public String getReplyto() {
		return replyto;
	}

	/**
	 * @param replyto the replyto to set
	 */
	public void setReplyto(String replyto) {
		this.replyto = replyto;
	}

	/**
	 * @return the returnPath
	 */
	public String getReturnPath() {
		return returnPath;
	}

	/**
	 * @param returnPath the returnPath to set
	 */
	public void setReturnPath(String returnPath) {
		this.returnPath = returnPath;
	}

	/**
	 * @return the to
	 */
	public String[] getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 * @throws AddressException
	 */
	public void setTo(String to) throws AddressException {
		this.to = parseReceipeintStr(to);
	}

	/**
	 * @return the cc
	 */
	public String[] getCc() {
		return cc;
	}

	/**
	 * @param cc the cc to set
	 * @throws AddressException
	 */
	public void setCc(String cc) throws AddressException {
		this.cc = parseReceipeintStr(cc);
	}

	/**
	 * @return the bcc
	 */
	public String[] getBcc() {
		return bcc;
	}

	/**
	 * @param bcc the bcc to set
	 * @throws AddressException
	 */
	public void setBcc(String bcc) throws AddressException {
		this.bcc = parseReceipeintStr(bcc);
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the txtPart
	 */
	public String getTxtPart() {
		return txtPart;
	}

	/**
	 * @param txtPart the txtPart to set
	 */
	public void setTxtPart(String txtPart) {
		this.txtPart = txtPart;
	}

	/**
	 * @return the htmlPart
	 */
	public String getHtmlPart() {
		return htmlPart;
	}

	/**
	 * @param htmlPart the htmlPart to set
	 */
	public void setHtmlPart(String htmlPart) {
		this.htmlPart = htmlPart;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String retVal = "<table>\r\n" + 
				"	<tbody>\r\n" + 
				"		<tr>\r\n" + 
				"			<th>From:</th>\r\n" + 
				"			<td>" + fromName + "&lt;" + fromMail  + "&gt;</td>\r\n" + 
				"		</tr>\r\n" + 
				"		<tr>\r\n" + 
				"			<th>To:</th>\r\n" + 
				"			<td class=\"toRecipientsSelector\">"+ Arrays.toString(to) +"</td>\r\n" + 
				"		</tr>\r\n" +
				"		<tr>\r\n" + 
				"			<th>cc:</th>\r\n" + 
				"			<td class=\"toRecipientsSelector\">"+ Arrays.toString(cc) +"</td>\r\n" + 
				"		</tr>\r\n" +
				"		<tr>\r\n" + 
				"			<th>bcc:</th>\r\n" + 
				"			<td class=\"toRecipientsSelector\">"+ Arrays.toString(bcc) +"</td>\r\n" + 
				"		</tr>\r\n" +  
				"		<tr>\r\n" + 
				"			<th>Subject:</th>\r\n" + 
				"			<td>"+ subject +"</td>\r\n" + 
				"		</tr>\r\n" + 
				"		<tr>\r\n" + 
				"			<th>Text Part:</th>\r\n" + 
				"			<td><pre>"
				+ txtPart +
				"			</pre></td>\r\n" + 
				"		</tr>\r\n" + 
				"		<tr>\r\n" + 
				"			<th>HTML Part:</th>\r\n" + 
				"			<td><pre>"
				+ htmlPart +
				"			</pre></td>\r\n" + 
				"		</tr>\r\n" +"	</tbody>\r\n" + 
				"</table>";
		
		return retVal ;
		
//				"EMail [from=" + from + ", replyto=" + replyto + ", returnPath=" + returnPath + ", subject=" + subject;
//				+ ", txtPart=" + txtPart + ", htmlPart=" + htmlPart + ", to=" + Arrays.toString(to) + ", cc="
//				+ Arrays.toString(cc) + ", bcc=" + Arrays.toString(bcc) + "]";
	}

	
}
