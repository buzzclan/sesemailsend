package com.buzz.taq.email.SESMailSend;

import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.buzz.taq.email.SESEmail;
import com.buzz.taq.email.entity.SendMail;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class SESMail
 */
@WebServlet(description = "Send eMail via AWS SES", urlPatterns = { "/SESMail" }, initParams = {
		@WebInitParam(name = "from", value = "", description = "From Address"),
		@WebInitParam(name = "replyTO", value = "", description = "Reply TO Address"),
		@WebInitParam(name = "to", value = "", description = "To Address"),
		@WebInitParam(name = "cc", value = "", description = "cc Address"),
		@WebInitParam(name = "bcc", value = "", description = "bcc Address"),
		@WebInitParam(name = "txtBody", value = "", description = "text body of the email"),
		@WebInitParam(name = "subject", value = "", description = "Subject of the eMail"),
		@WebInitParam(name = "htmlBody", value = "", description = "HTML Body of the eMail") })
public class SESMail extends HttpServlet {
	private static final Logger LOGGER = LogManager.getLogger(SESMail.class.getName());

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SESMail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SendMail sm = readBody(request);
		String retMsg = "{\"message\":\"?\",\"msgID\":\"#\"}";
		if (sm.isSent()) {
			retMsg = retMsg.replace("?", "Message Sent");
			retMsg = retMsg.replace("#", sm.getMsgID());
			response.getWriter().append(retMsg);
			response.setStatus(HttpServletResponse.SC_OK);
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				LOGGER.error("The email was not sent. Error message:" + sm.getMsgID());
				response.sendError(400, "The email was not sent. Error message:" + sm.getMsgID());
			} catch (IOException io) {

			}
			// retVal = "{\"message\":\"The email was not sent. Error message:" +
			// ae.getMessage() + "\",\"msgID\":\"0\"";
		}

		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	private SendMail readBody(HttpServletRequest httpRequest) throws IOException {
		String JSONStr = httpRequest.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

		LOGGER.trace(JSONStr);

		ObjectMapper mapper = new ObjectMapper();

		// JSON file to Java object
		SendMail sm = mapper.readValue(JSONStr, SendMail.class);

		sm = SESEmail.sendEmail(sm);
		LOGGER.trace(sm.getMsgID());

		return sm;// ;httpRequest.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
