
package com.buzz.taq.email.entity;

import java.util.List;

import com.buzz.taq.emailResopnse.entity.MsgBase;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "configSet", "subject", "txtBody", "htmlBody", "from", "to", "cc", "bcc", "returnPath" })
public class SendMail extends MsgBase {

	@JsonProperty("configSet")
	private String configSet;
	@JsonProperty("subject")
	private String subject;
	@JsonProperty("txtBody")
	private String txtBody;
	@JsonProperty("htmlBody")
	private String htmlBody;
	@JsonProperty("from")
	private From from;
	@JsonProperty("to")
	private List<To> to = null;
	@JsonProperty("cc")
	private List<Cc> cc = null;
	@JsonProperty("bcc")
	private List<Bcc> bcc = null;
	@JsonProperty("returnPath")
	private String returnPath;

	private String msgID ="Error while sending eMail";
	public String getMsgID() {
		return msgID;
	}

	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

	private boolean sent = false;
	@JsonProperty("configSet")
	public String getConfigSet() {
		if (configSet == null || configSet.isEmpty()) {
			configSet = "ConfigSet";
		}
		return configSet;
	}

	@JsonProperty("configSet")
	public void setConfigSet(String configSet) {
		this.configSet = configSet;
	}

	public SendMail withConfigSet(String configSet) {
		this.configSet = configSet;
		return this;
	}

	@JsonProperty("subject")
	public String getSubject() {
		return subject;
	}

	@JsonProperty("subject")
	public void setSubject(String subject) {
		this.subject = subject;
	}

	public SendMail withSubject(String subject) {
		this.subject = subject;
		return this;
	}

	@JsonProperty("txtBody")
	public String getTxtBody() {
		return txtBody;
	}

	@JsonProperty("txtBody")
	public void setTxtBody(String txtBody) {
		this.txtBody = txtBody;
	}

	public SendMail withTxtBody(String txtBody) {
		this.txtBody = txtBody;
		return this;
	}

	@JsonProperty("htmlBody")
	public String getHtmlBody() {
		return htmlBody;
	}

	@JsonProperty("htmlBody")
	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}

	public SendMail withHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
		return this;
	}

	@JsonProperty("from")
	public String getFrom() {
		return from.toString();
	}

	@JsonProperty("from")
	public void setFrom(From from) {
		this.from = from;
	}

	public SendMail withFrom(From from) {
		this.from = from;
		return this;
	}

	@JsonProperty("to")
	public String[] getTo() {
		String[] retVal = null;
		if (to != null) {
			retVal = new String[to.size()];
			int i = 0;
			for (Email ccO : to) {
				retVal[i] = ccO.toString();
				i++;
			}
		}
		return retVal;
	}

	@JsonProperty("to")
	public void setTo(List<To> to) {
		this.to = to;
	}

	public SendMail withTo(List<To> to) {
		this.to = to;
		return this;
	}

	@JsonProperty("cc")
	public String[] getCc() {
		String[] retVal = null;
		if (cc != null) {
			retVal = new String[cc.size()];
			int i = 0;
			for (Email ccO : cc) {
				retVal[i] = ccO.toString();
				i++;
			}
		}
		return retVal;
	}

	@JsonProperty("cc")
	public void setCc(List<Cc> cc) {
		this.cc = cc;
	}

	public SendMail withCc(List<Cc> cc) {
		this.cc = cc;
		return this;
	}

	@JsonProperty("bcc")
	public String[] getBcc() {
		String[] retVal = null;
		if (bcc != null) {
			retVal = new String[bcc.size()];
			int i = 0;
			for (Email ccO : bcc) {
				retVal[i] = ccO.toString();
				i++;
			}
		}
		return retVal;
	}

	@JsonProperty("bcc")
	public void setBcc(List<Bcc> bcc) {
		this.bcc = bcc;
	}

	public SendMail withBcc(List<Bcc> bcc) {
		this.bcc = bcc;
		return this;
	}

	@JsonProperty("returnPath")
	public String getReturnPath() {
		if (returnPath == null || returnPath.isEmpty()) {
			if (from != null) {
				returnPath = from.getEmail();
			}
		}
		return returnPath;
	}

	@JsonProperty("returnPath")
	public void setReturnPath(String returnPath) {
		this.returnPath = returnPath;
	}

	public SendMail withReturnPath(String returnPath) {
		this.returnPath = returnPath;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("configSet", configSet).append("subject", subject)
				.append("txtBody", txtBody).append("htmlBody", htmlBody).append("from", from).append("to", to)
				.append("cc", cc).append("bcc", bcc).append("returnPath", returnPath).toString();
	}

}
