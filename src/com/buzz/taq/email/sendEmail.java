package com.buzz.taq.email;

import java.io.IOException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class sendEmail
 */
@WebServlet(description = "Sends regular eMail", urlPatterns = { "/sendEmail" }, initParams = {
		@WebInitParam(name = "from", value = "", description = "From Address"),
		@WebInitParam(name = "to", value = "", description = "To Address"),
		@WebInitParam(name = "cc", value = "", description = "To Address"),
		@WebInitParam(name = "bcc", value = "", description = "To Address"),
		@WebInitParam(name = "txtBody", value = "", description = "text body of the email"),
		@WebInitParam(name = "subject", value = "", description = "Subject of the eMail"),
		@WebInitParam(name = "htmlBody", value = "", description = "HTML Body of the eMail") })
public class sendEmail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public sendEmail() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().append(processEmail(request, response));
		response.getWriter().append("<br/>Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private String processEmail(HttpServletRequest httpRequest, HttpServletResponse response) {
		String retVal = "";
		String to = httpRequest.getParameter("to");
		String cc = httpRequest.getParameter("cc");
		String bcc = httpRequest.getParameter("bcc");
		String from = httpRequest.getParameter("from");
		String subject = httpRequest.getParameter("subject");
		String txtBody = httpRequest.getParameter("txtbody");
		String HTMLBody = httpRequest.getParameter("htmlbody");
		try {
			response.getWriter().append("<br/>From:").append(from).append("<br/>TO:").append(to).append("<br/>cc:")
					.append(cc).append("<br/>bcc:").append(bcc).append("<br/>txtbody:<pre>").append(txtBody)
					.append("</pre><br/>htmlbody:<pre>").append(HTMLBody).append("</pre>");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			retVal = SESEmail.sendEmail(subject, txtBody, HTMLBody, from, to, cc, bcc);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (AddressException ae) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				response.sendError(400, "The email was not sent. Error message:" + ae.getMessage());
			} catch (IOException io) {
			}
			retVal = "The email was not sent. Error message:" + ae.getMessage();
		}

		return retVal;
	}

}
